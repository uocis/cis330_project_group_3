# README.md
Welcome to the CIS 330 Final Project Game Repository.

# Abstract
Over the course of the term, we have created a checkers game in C++ which begins by displaying
the checkers board on the command line. The user is prompted to select a tile and then to select the
corresponding movement for that tile. The game continues until one of the players surrenders or simply
has had all of their tiles captured. There is a built in fixed pattern algorithm for aiding the user in
choosing a best tile to select and movement for that tile. The algorithm considers all possible
movements for each tile and then selects the move with the highest rating. The moves are rated based
on the likelihood of being captured, capturing another tile, and the security of the location. It's only OK
at doing its job though, it doesn't look very far into the future but maybe in the future we can make it do
that.

## Compilation of Game
1. Clone the repository from bitbucket.
1. `cd cis330_project_group_3`
1. `cmake CMakeLists.txt`
1. `make`
1. `./Checkers`

## Compilation of UnitTests
1. This is included within the main repo so please ensure that you have completed step 1. from Compilation of Game, to clone. 
1. `cd cis330_project_group_3`
1. `mkdir vendor`
1. Inside the vendor directory is where the gTest libraries will be put, the full path is specified in the CMakeLists.txt within the tests/ directory. 
1. `cmake CMakeLists.txt`
1. `make`
1. `./CheckersUT`
 
