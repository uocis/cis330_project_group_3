//
// Created by Ryan Gurnick on 2019-05-21.
//
#include <iostream>
#include <stdio.h>
#include <strings.h>
#include "Board.h"


#ifndef GAME_CLI_H
#define GAME_CLI_H

using namespace std;

class CLI {
private:
    // current player that is able to select
    int currentPlayer;
    // current move x
    int currentMoveVecX;
    // current move y
    int currentMoveVecY;

    // the board itself
    Board *board = new Board;
    // has it been started?
    bool gameStarted = false;

    // initial player names
    string player0Name = "Jee";
    string player1Name = "Chris";

    /**
     * builds header (buildHeader)
     * prints the initial header for the game
     */
    void buildHeader();
    /**
     * sets players (setPlayers)
     * players select their name and the player who makes the first move is determined
     * @return true
     */
    bool setPlayers();
    /**
     *gets selection from user (getSelection)
     * Users get to select whether they wish to start or exit the game
     * @return
     */
    int getSelection();
    /**
     * informs player that it is their turn to move (tellCurrentPlayer)
     * @param stupid
     */
    void tellCurrentPlayer(bool stupid = false);
    /**
     * prompts user to select a tile (requestMove)
     * determines if tileSelected is a tile that can be moved
     */
    void requestMove();
    /**
     * requests the direction (requestMoveVector)
     * this will use the possible moves to provide
     * area for user input in the game.
     */
    void requestMoveVector();


public:
    /**
     * Constructor
     */
    CLI();

    /**
     * Destructor
     */
    ~CLI();

    /**
     * Start everything (createMenu())
     */
    void createMenu();
};


#endif //GAME_CLI_H
