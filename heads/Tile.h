//
// Created by Ryan Gurnick on 2019-05-08.
//

#include <iostream>
#include <stdio.h>

using namespace std;

#ifndef GAME_TILE_H
#define GAME_TILE_H


class Tile {
private:

public:
    int     x;              // the curr x
    int     y;              // the curr y
    int     startX;         // the origional x
    int     startY;         // the origional y
    bool    valid = true;   // all tiles start valid
    bool    king = false;   // all tiles start not a king
    int     player;         // which player does this belong to
    string  name;           // whats the tiles name

    /**
     * Tile constructor
     * @param x
     * @param y
     * @param player
     */
    Tile(int x, int y, int player);

    /**
     * Print tile (printTile)
     * This will print the tile information
     * mainly for debugging.
     * @return
     */
    string printTile();

    /**
     * Updates the Tile Name (updateTileName)
     * This will take the current x and current y
     * and use the normal form to set the new name.
     *
     * @param void
     * @return void
     */
    void updateTileName();

    /**
     * Gives king ability (makeKing)
     * This will allow the current tile to have
     * king abilities, it will change the public
     * value that is defaulted to false
     *
     * @return void
     */
    void makeKing();

    /**
     * Check kinglyness (isKing)
     * This will allow you to check if the current
     * tile is in fact a king.
     * @return bool
     */
    bool isKing();

    /**
     * moves tile to new location specified by the input (moveTile)
     * When a tile is moved, the tile name is updated so it matches the new location.
     * @param dirX
     * @param dirY
     * @param dirBias
     * @return
     */
    bool moveTile(int dirX, int dirY, int dirBias);

};


#endif //GAME_TILE_H
